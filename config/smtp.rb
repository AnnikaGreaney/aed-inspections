SMTP_SETTINGS = {
  address:              "smtp.sendgrid.net",
  authentication:       :plain,
  enable_starttls_auto: true,
  password:             ENV["SENDGRID_PASSWORD"],
  port:                 "587",
  user_name:            ENV["SENDGRID_USERNAME"]
}.freeze

# frozen_string_literal: true

# Monthly Inspections are updated once a month with data about an AED
class MonthlyInspection < ApplicationRecord
  belongs_to :aed
  belongs_to :user

  has_one :cabinet_status_information, dependent: :destroy
  has_one :aed_status_information, dependent: :destroy

  accepts_nested_attributes_for :cabinet_status_information
  accepts_nested_attributes_for :aed_status_information

  enum cabinet_status: %i[not_applicable good damaged unable_to_check]
  enum aed_unit_status: %i[not_present out_of_service working needs_maintenance]
  enum rescue_kit_status: %i[present needs_attention]
end

# frozen_string_literal: true

# Static Pages
class PagesController < ApplicationController
  def index
    @months = MonthlyInspection.all.pluck(:year, :month).uniq
  end
end

# frozen_string_literal: true

# Record Monthly Inspection Data for an AED
class MonthlyInspectionsController < ApplicationController
  before_action :cabinet_statuses, only: %i[new edit]
  before_action :aed_unit_statuses, only: %i[new edit]
  before_action :rescue_kit_statuses, only: %i[new edit]
  before_action :aeds, only: %i[new edit]

  def index
    respond_to do |format|
      format.html { @inspections = MonthlyInspection.all.order(year: :desc, month: :desc) }
      format.pdf do
        @reports = MonthlyInspection.where(year: params[:year], month: params[:month])
        render pdf:         "aed_inspection_#{params[:month]}_#{params[:year]}",
               disposition: "attachment",
               layout:      "pdf.html.erb",
               orientation: "landscape",
               template:    "monthly_inspections/show.pdf.erb",
               locals:      { reports: @reports }
      end
    end
  end

  def new
    @monthly_inspection = MonthlyInspection.new
    @monthly_inspection.build_cabinet_status_information
    @monthly_inspection.build_aed_status_information
  end

  def edit
    @monthly_inspection = MonthlyInspection.find(params[:id])
  end

  def show
    @monthly_inspection = MonthlyInspection.find(params[:id])
  end

  def create
    inspection = create_inspection(inspection_params)
    inspection.create_cabinet_status_information!(information: inspection_params[:cabinet_status_information_attributes][:information])
    inspection.create_aed_status_information!(information: inspection_params[:aed_status_information_attributes][:information])
    if inspection
      redirect_to monthly_inspections_path, notice: "Monthly Inspection was recorded."
    else
      redirect_to monthly_inspections_path, alert: "Monthly Inspection could not be saved."
    end
  end

  def update
    inspection = MonthlyInspection.find(params[:id])
    inspection.update(inspection_params)
    #inspection.update_cabinet_status_information!(information: inspection_params[:cabinet_status_information_attributes][:information])
    #inspection.update_aed_status_information!(information: inspection_params[:aed_status_information_attributes][:information])
    if inspection
      redirect_to monthly_inspections_path, notice: "Monthly Inspection was recorded."
    else
      redirect_to monthly_inspections_path, alert: "Monthly Inspection could not be saved."
    end
  end


private

  def inspection_params
    params.require(:monthly_inspection).permit(
      :year, :month, :aed_id, :cabinet_status,
      :aed_unit_status, :rescue_kit_status,
      :comments,
      cabinet_status_information_attributes: [:information],
      aed_status_information_attributes:     [:information]
    )
  end

  def inspection
    @monthly_inspection = MonthlyInspection.find(params[:id])
  end

  def aeds
    @aeds = Aed.all.map {|a| [a.name, a.id] }
  end

  def cabinet_statuses
    @cabinet_statuses = MonthlyInspection.cabinet_statuses.map {|s| [s[0].humanize, s[0]] }
  end

  def aed_unit_statuses
    @aed_unit_statuses = MonthlyInspection.aed_unit_statuses.map {|s| [s[0].humanize, s[0]] }
  end

  def rescue_kit_statuses
    @rescue_kit_statuses = MonthlyInspection.rescue_kit_statuses.map {|s| [s[0].humanize, s[0]] }
  end

  def create_inspection(inspection_params)
    MonthlyInspection.create!(
      month: inspection_params[:month], date: Date.today.day, comments: inspection_params[:comments],
      aed_id: inspection_params[:aed_id], cabinet_status: inspection_params[:cabinet_status],
      aed_unit_status: inspection_params[:aed_unit_status], user_id: current_user.id,
      rescue_kit_status: inspection_params[:rescue_kit_status], year: Date.today.year
    )
  end
end

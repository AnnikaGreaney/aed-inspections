class CreateCabinetStatusInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :cabinet_status_informations do |t|
      t.references :monthly_inspection, foreign_key: true
      t.text :information
    end
  end
end

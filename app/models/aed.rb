class Aed < ApplicationRecord
  has_many :monthly_inspections, dependent: :destroy
  default_scope {order(name: :asc)}
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_23_212236) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "aed_status_informations", force: :cascade do |t|
    t.bigint "monthly_inspection_id"
    t.text "information"
    t.index ["monthly_inspection_id"], name: "index_aed_status_informations_on_monthly_inspection_id"
  end

  create_table "aeds", force: :cascade do |t|
    t.string "name"
    t.string "model"
    t.string "serial_number"
    t.string "location"
    t.datetime "date_of_purchase"
    t.datetime "battery_expiration"
    t.datetime "pad_1_expiration"
    t.datetime "pad_2_expiration"
    t.datetime "pediatric_pad_expiration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cabinet_status_informations", force: :cascade do |t|
    t.bigint "monthly_inspection_id"
    t.text "information"
    t.index ["monthly_inspection_id"], name: "index_cabinet_status_informations_on_monthly_inspection_id"
  end

  create_table "monthly_inspections", force: :cascade do |t|
    t.integer "year"
    t.integer "month"
    t.integer "date"
    t.bigint "aed_id"
    t.bigint "user_id"
    t.integer "cabinet_status"
    t.integer "aed_unit_status"
    t.integer "rescue_kit_status"
    t.text "comments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aed_id"], name: "index_monthly_inspections_on_aed_id"
    t.index ["user_id"], name: "index_monthly_inspections_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "type"
    t.string "first_name"
    t.string "last_name"
    t.string "initials"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "aed_status_informations", "monthly_inspections"
  add_foreign_key "cabinet_status_informations", "monthly_inspections"
  add_foreign_key "monthly_inspections", "aeds"
  add_foreign_key "monthly_inspections", "users"
end

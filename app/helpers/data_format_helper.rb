module DataFormatHelper
  def format_date(date)
    if date
      date.strftime("%D")
    else
      ""
    end
  end
end

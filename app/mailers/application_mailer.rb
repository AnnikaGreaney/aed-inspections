class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  def pads_expired(pads)
    email = User.first.email
    @pads = pads
    mail(to: email, subject: "AED Pads Expiring Soon")
  end
end

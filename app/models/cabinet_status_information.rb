class CabinetStatusInformation < ApplicationRecord
  belongs_to :monthly_inspections, optional: true
end

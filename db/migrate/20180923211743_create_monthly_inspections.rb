class CreateMonthlyInspections < ActiveRecord::Migration[5.2]
  def change
    create_table :monthly_inspections do |t|
      t.integer :year
      t.integer :month
      t.integer :date
      t.references :aed, foreign_key: true
      t.references :user, foreign_key: true
      t.integer :cabinet_status
      t.integer :aed_unit_status
      t.integer :rescue_kit_status
      t.text :comments
      t.timestamps
    end
  end
end

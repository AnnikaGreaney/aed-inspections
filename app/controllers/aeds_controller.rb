# frozen_string_literal: true

# AED is the main object used within this application
class AedsController < ApplicationController
  before_action :aed, except: %i[index new create]

  def index
    @aeds = Aed.all
  end

  def create
    if Aed.create!(aed_params)
      redirect_to aeds_path, notice: "AED was created."
    else
      redirect_to aeds_path, alert: "AED could not be created."
    end
  end

  def update
    if @aed.update(aed_params)
      redirect_to aeds_path, notice: "AED was updated."
    else
      redirect_to aeds_path, alert: "AED could not be updated."
    end
  end

  def destroy
    if @aed.destroy!
      redirect_to root_path, notice: "AED was deleted."
    else
      redirect_to root_path, alert: "AED could not be deleted."
    end
  end

private

  def aed
    @aed = Aed.find(params[:id])
  end

  def aed_params
    params.require(:aed).permit(
      :battery_expiration, :pad_1_expiration,
      :pad_2_expiration, :pediatric_pad_expiration,
      :date_of_purchase, :name, :model, :location,
      :serial_number
    )
  end
end

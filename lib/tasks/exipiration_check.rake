namespace :expirations do
  desc "Alert on expiring pads"
  task aed: :environment do
    aeds = Aed.all
    notify = {}
    aeds.each do |a|
      aed = []
      aed.push "Pad 1" if a.pad_1_expiration.present? && (a.pad_1_expiration - 30.days) < Date.today
      aed.push "Pad 2" if a.pad_2_expiration.present? && (a.pad_2_expiration - 30.days) < Date.today
      aed.push "Pediatric" if a.pediatric_pad_expiration.present? && (a.pediatric_pad_expiration - 30.days) < Date.today
      notify[a.name] = aed unless aed.empty?
    end
    ApplicationMailer.pads_expired(notify).deliver_now unless notify.empty?
  end
end

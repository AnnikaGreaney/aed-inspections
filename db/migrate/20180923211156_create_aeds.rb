class CreateAeds < ActiveRecord::Migration[5.2]
  def change
    create_table :aeds do |t|
      t.string :name
      t.string :model
      t.string :serial_number
      t.string :location
      t.datetime :date_of_purchase
      t.datetime :battery_expiration
      t.datetime :pad_1_expiration
      t.datetime :pad_2_expiration
      t.datetime :pediatric_pad_expiration
      t.timestamps
    end
  end
end
